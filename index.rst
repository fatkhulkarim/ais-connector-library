WakatobiAIS (BAKTI) Bluetooth Interface
Overview
Note to readers (1): this page only applies to WakatobiAIS (BAKTI)

Note to readers (2): The frame structures and communication protocols explained below are not final. Critics and improvement suggestions are very welcomed!

BAKTI mobile application requires the AIS equipment to have three functionality:

Tracking
Tagging
Search and Rescue / Emergency
Tracking functionality requires the AIS equipment to periodically transmit position information along with other additional information such as ship's name, MMSI number, and call sign. Additionally, this functionality requires the AIS equipment to provide position information (latitude and longitude) to a Bluetooth connected BAKTI mobile application when requested. Tagging functionality requires the AIS equipment to be capable of receiving encrypted tagging information from the Bluetooth connected BAKTI mobile application at all times. This encrypted tagging information will then be transmitted by the AIS equipment through AIS message 8 (binary broadcast message). Search and rescue / Emergency functionality requires the AIS equipment to be capable of being switched into AIS-SART (AIS Search and Rescue Transponder) at all times (preferably at emergency situations), sending bursts of position information and emergency related broadcast message every minute. Content of the emergency related broadcast message should be able to be chosen through the Bluetooth connected BAKTI mobile application. Moreover, switching into AIS-SART mode should be able to be triggered through the BAKTI mobile application.

Therefore, a standard frame structure and simple communication protocol has been developed for the communication between WakatobiAIS and BAKTI mobile application through a Bluetooth interface.

Bluetooth Low Energy (BLE) Protocol
From all the Bluetooth versions available, we have chosen BLE because of its low power usage that is of paramount importance for the AIS equipment. BLE have what is called services and characteristics. Basically they are information container that can either be read, written, or read and written. For example, the generic access service (0x1800) have a device name characteristic (0x2A00) that can be read.

The Bluetooth module used in WakatobiAIS (HM-10 module with CC2540/CC2541 TI BLE chip) also has a custom service (0xFFE0) with a custom characteristic (0xFFE1) that can be read, written, and is notifiable (The connected device is notified when the value changed). The communication between WakatobiAIS and BAKTI mobile application utilizes this custom service/characteristic. Sending a message, either from WakatobiAIS or BAKTI mobile application, is done by writing the message to the custom characteristic value while receiving a message, either from WakatobiAIS or BAKTI mobile application, is done by reading the message from the custom characteristic value.

However, the reader must be aware that a BLE characteristic value is only 20 octets wide. Therefore messages that are wider then 20 octets wide must be segmented in to 20 octets wide chunks and transmitted (written to the custom characteristics field) sequentially.

Message Frame Structure
images/BT-frame-structure.png
The start of frame byte signifies the beginning of a valid message. This is important for the receiver to resynchronize to a start of a valid message when errors exist in previous messages. Message classification byte is required by the receiver to know what information is contained in the payload. For example, a message classification byte of A (0x41) indicates that the payload is a string of characters that will be transmitted inside the emergency broadcast message. This will be explained more thoroughly for each message class later. The end of frame byte signifies the end of the payload segment and may be important if the payload size is allowed to be variable for certain messages. Moreover, this redundancy decreases the chance of encountering false positives. The last two byte is the checksum of the message written in 2 ASCII character corresponding to the checksum value written in uppercase hexadecimal value.

Tracking Functionality
To request the latitude and longitude value from WakatobiAIS, the following message must be sent:

images/GPS-request-frame-structure.png
After WakatobiAIS received the above message while however the GPS receiver on WakatobiAIS has not yet locked on to a location, it will respond with the following message:

images/void-GPS-respond-frame-structure.png
However, if the GPS receiver has already locked on to a location, it will respond with the following message:

images/locked-GPS-respond-frame-structure.png
Dl1 Dl2 are the two digits of latitude's degree value, while Ml1 Ml2 Ml3 Ml4 are the four digits of the latitude's minute value in 1/100th of a minute. 'N' / 'S' referes to the latitude's direction, either north or south. For example, if 'Dl1' = '0', 'Dl2' = '6', 'Ml1' = '2', 'Ml2' = '3', 'Ml3' = '1', 'Ml4' = '6', and the direction is 'N', the overall latitude value is 6'23.16"N. Interpretation of the longitude fields follows the same exact rule, the only difference is the additional hundreds position for the degree value.

Search and Rescue / Emergency Functionality
To switch WakatobiAIS in to AIS-SART mode, the following message must be sent:

images/message-alert-request-frame-structure.png
The payload should be filled with 16 characters that would be sent inside the emergency-related broadcast message. If the message is shorter than 16 characters, it should be filled with spaces (ASCII 32). Only ASCII characters listed in the table below (with the exeption of '!' and '*') are allowed inside the payload.

images/ascii-table.png
In addition to missing start and end of frame byte ('!' and '*') and mismatched checksum, failure to comply with the constraint mentioned above will result in ignored/dropped request. If the message was not ignored/dropped, WakatobiAIS will respond with the following confirmation message:

images/good-alert-respond-frame-structure.png
A timeout of around 500 ms should be employed in the mobile application side while waiting for the above response. If the timeout is exceeded, the request should be repeated until the response above is acquired. After the request is confirmed, there is a 10 s timeout before the actual switching to AIS-SART mode truly begins. In this time frame, the user can request to cancel the switching action by sending the following message:

images/cancel-alert-request-frame-structure.png
If the cancel request is not ignored/dropped, WakatobiAIS will respond with the same message as the cancel request above to the mobile application. As before, timeout mechanism should be employed to make sure the cancel request will be executed.

Tagging Functionality
(Under construction)

Checksum Calculation
The checksum calculation method employed is the parity word checksum. To calculate the checksum value, simply XOR every byte in the message (including ! and *). The acquired 8-bit checksum value hexadecimal representation in 2 ASCII characters are then appended to the end of the message. For example, if the checksum value is 0xA5, then the last two characters of the message should be A and 5. The following algorithm written in Python can be used as a reference:

def bitToHexChar(x):
   # Convert a 4 bit value to its hexadecimal representation in ASCII
   if(x < 10):
      return chr(x + ord('0'))
   else:
      return chr(x - 10 + ord('A'))

# Switch to AIS-SART mode request
characters = ['!','A','E','M','P','T','Y',' ','F','U','E','L',' ',' ',' ',' ',' ',' ','*','','']
checksum = 0
for i in range(len(characters)-2):
   checksum = checksum ^ ord(characters[i])
characters[len(characters)-2] = bitToHexChar(checksum >> 4)
characters[len(characters)-1] = bitToHexChar(checksum & 0x0f)

# Result : characters = ['!','A','E','M','P','T','Y',' ','F','U','E','L',' ',' ',' ',' ',' ',' ','*','2','5']